A simple prediction model that uses linear regression.

To use the model, simply input the relevant information in the input.xlsx file, and then run the learning-model.py script in the src folder.
