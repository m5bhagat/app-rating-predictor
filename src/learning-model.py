import numpy as np
import matplotlib.pyplot as plot
import datetime as dt
import pandas as pd
from sklearn.linear_model import LinearRegression

#Global Variables
trainingdata = pd.read_csv("../data/clean_data.csv")
userinput = pd.read_excel("../input.xlsx")

formattedinput = pd.DataFrame(columns = trainingdata.columns)
del formattedinput['Rating']
for index, row in userinput.iterrows():
    newRow = pd.DataFrame(columns = formattedinput.columns)
    newRow = newRow.append({"Reviews" : row["Reviews"]}, ignore_index = True)
    newRow[:].values[:] = 0
    newRow["Reviews"] = row["Reviews"]
    newRow["Installs"] = row["Installs"]
    newRow["Price"] = row["Price"]
    newRow["Last_Updated"] = row["Last_Updated"]
    newRow[row["Category"]] = 1
    newRow[row["Content_Rating"]] = 1

    formattedinput = formattedinput.append(newRow, ignore_index = True)

m = trainingdata.shape[0]
X = np.array(trainingdata.loc[ :, trainingdata.columns != "Rating"])
y = np.array(trainingdata.loc[ :, "Rating"])

reg = LinearRegression().fit(X, y)
reg.score(X, y)

reg.coef_

reg.intercept_ 

print("Predictions:")
i = 0
for index, row in formattedinput.iterrows():
    print( userinput.loc[i]["App_Name"] + " would have a rating of " + str(reg.predict(np.array(row).reshape(1, -1))))
    i += 1

#Code for backtesting
'''
Actual = y
Predicted = np.array(y)

i = 0
while ( i < np.size(Predicted)):
    Predicted[i] = reg.predict(np.array(X[i].reshape(1, -1)))
    i += 1

plot.scatter(Actual, Predicted)
plot.xlabel('Actual')
plot.ylabel('Predicted')
plot.show()
'''